import { appReducers } from './app.state/app.reducer';
import { ProductManagerEffects } from './modules/product-manager/product-manager.state/product-manager.effect';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductManagerComponent } from './modules/product-manager/product-manager.component';
import { ContentBodyComponent } from './partials/content-body/content-body.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ProductModalComponent } from './modules/product-manager/components/product-modal/product-modal.component';
import { HomeComponent } from './modules/home/home.component';
import { StoreModule } from '@ngrx/store';
import { productManagerModule } from './modules/product-manager/product-manager.state/product-manager.reducer';
import { categoryManagerModule } from './modules/category-manager/category-manager.state/category-manager.reducer';
import { EffectsModule } from '@ngrx/effects';
import { CategoryManagerEffects } from './modules/category-manager/category-manager.state/category-manager.effect';
import { HeaderComponent } from './partials/header/header.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LoadingEffectComponent } from './modules/shared/component/loading-effect/loading-effect.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    ProductManagerComponent,
    ContentBodyComponent,
    ProductModalComponent,
    HomeComponent,
    HeaderComponent,
    LoadingEffectComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
    ModalModule.forRoot(),
    StoreModule.forRoot(appReducers),
    EffectsModule.forRoot([ProductManagerEffects, CategoryManagerEffects]),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
