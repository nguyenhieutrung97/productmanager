import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductManagerComponent } from './modules/product-manager/product-manager.component';
import { HomeComponent } from './modules/home/home.component';
import { ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  { path: 'product-manager', component: ProductManagerComponent },
  { path: '', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), ReactiveFormsModule],
  exports: [RouterModule],
})
export class AppRoutingModule {}
