import { ILoadingEffectState } from './../modules/shared/component/loading-effect/loading-effect.state/loading-effect.state';
import { ICategoryManagerState } from './../modules/category-manager/category-manager.state/category-manager.state';
import { IProductManagerState } from './../modules/product-manager/product-manager.state/product-manager.state';
import { RouterReducerState } from '@ngrx/router-store';
export interface IAppState {
  router?: RouterReducerState;
  productManager: IProductManagerState;
  categoryManager: ICategoryManagerState;
  loadingState: ILoadingEffectState;
}
