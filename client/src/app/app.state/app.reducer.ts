import {
  initialLoadingEffectState,
  loadingEffectModule,
} from './../modules/shared/component/loading-effect/loading-effect.state/loading-effect.reducer';
import {
  initialCategoryManagerState,
  categoryManagerModule,
} from './../modules/category-manager/category-manager.state/category-manager.reducer';
import { IAppState } from './app.state';
import { ActionReducerMap } from '@ngrx/store';
import { routerReducer } from '@ngrx/router-store';
import {
  initialProductManagerState,
  productManagerModule,
} from '../modules/product-manager/product-manager.state/product-manager.reducer';

export const initialAppState: IAppState = {
  productManager: initialProductManagerState,
  categoryManager: initialCategoryManagerState,
  loadingState: initialLoadingEffectState,
};

export const appReducers: ActionReducerMap<IAppState, any> = {
  router: routerReducer,
  productManager: productManagerModule,
  categoryManager: categoryManagerModule,
  loadingState: loadingEffectModule,
};
