import { ofType, Actions, createEffect } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { map, catchError, mergeMap, finalize } from 'rxjs/operators';
import {
  CategoryManagerActionNames,
  GetCategoriesActionSuccess,
  CategoryActionFailed,
} from './category-manager.action';

import { CategoryService } from './../../shared/service/category.service';
import { of } from 'rxjs';
import {
  OpenLoadingStateAction,
  CloseLoadingStateAction,
} from '../../shared/component/loading-effect/loading-effect.state/loading-effect.action';
import { Store } from '@ngrx/store';
import { IAppState } from 'src/app/app.state/app.state';

@Injectable()
export class CategoryManagerEffects {
  constructor(
    private actions$: Actions,
    private store: Store<IAppState>,
    private categoryService: CategoryService
  ) {}

  loadCategories$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CategoryManagerActionNames.GET_CATEGORIES_REQUEST),
      mergeMap(() => {
        this.store.dispatch(new OpenLoadingStateAction());
        return this.categoryService.getCategories().pipe(
          map((categories) => {
            return new GetCategoriesActionSuccess(categories);
          }),
          catchError((error) => of(new CategoryActionFailed(error.message))),
          finalize(() => {
            this.store.dispatch(new CloseLoadingStateAction());
          })
        );
      })
    )
  );
}
