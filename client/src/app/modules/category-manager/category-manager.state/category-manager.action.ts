import { Action } from '@ngrx/store';
import { Category } from '../../shared/model/category/category';

export enum CategoryManagerActionNames {
  GET_CATEGORIES_REQUEST = '[CATEGORY MANAGER] Get Categories Request',
  GET_CATEGORIES_SUCCESS = '[CATEGORY MANAGER] Get Categories Success',
  GET_CATEGORY_REQUEST = '[CATEGORY MANAGER] Get Category Request',
  GET_CATEGORY_SUCCESS = '[CATEGORY MANAGER] Get Category Success',
  ADD_CATEGORY_REQUEST = '[CATEGORY MANAGER] Add Category Request',
  ADD_CATEGORY_SUCCESS = '[CATEGORY MANAGER] Add Category Success',
  DELETE_CATEGORY_REQUEST = '[CATEGORY MANAGER] Delete Category Request',
  DELETE_CATEGORY_SUCCESS = '[CATEGORY MANAGER] Delete Category Success',
  UPDATE_CATEGORY_REQUEST = '[CATEGORY MANAGER] Update Category Request',
  UPDATE_CATEGORY_SUCCESS = '[CATEGORY MANAGER] Update Category Success',
  CATEGORIES_ACTION_FAILED = '[CATEGORY MANAGER] Get Categories Failed',
}
export class GetCategoryActionSuccess implements Action {
  type = CategoryManagerActionNames.GET_CATEGORY_SUCCESS;
  constructor(public payload: Category) {}
}

export class GetCategoriesActionRequest implements Action {
  type = CategoryManagerActionNames.GET_CATEGORIES_REQUEST;
  constructor() {}
}
export class GetCategoriesActionSuccess implements Action {
  type = CategoryManagerActionNames.GET_CATEGORIES_SUCCESS;
  constructor(public payload: Category[]) {}
}

export class AddCategoryActionRequest implements Action {
  type = CategoryManagerActionNames.ADD_CATEGORY_REQUEST;
  constructor() {}
}
export class AddCategoryActionSuccess implements Action {
  type = CategoryManagerActionNames.ADD_CATEGORY_SUCCESS;
  constructor(public payload: Category) {}
}
export class DeleteCategoryActionRequest implements Action {
  type = CategoryManagerActionNames.DELETE_CATEGORY_REQUEST;
  constructor() {}
}
export class DeleteCategoryActionSuccess implements Action {
  type = CategoryManagerActionNames.DELETE_CATEGORY_SUCCESS;
  constructor(public payload: Category) {}
}
export class UpdateCategoryActionRequest implements Action {
  type = CategoryManagerActionNames.UPDATE_CATEGORY_REQUEST;
  constructor() {}
}
export class UpdateCategoryActionSuccess implements Action {
  type = CategoryManagerActionNames.UPDATE_CATEGORY_SUCCESS;
  constructor(public payload: Category) {}
}
export class CategoryActionFailed implements Action {
  type = CategoryManagerActionNames.CATEGORIES_ACTION_FAILED;
  constructor(public payload: any) {}
}
