import { Action } from '@ngrx/store';
import { CategoryManagerActionNames } from './category-manager.action';
import { ICategoryManagerState } from './category-manager.state';
import { Category } from '../../shared/model/category/category';

export interface CategoryManagerAction extends Action {
  type: CategoryManagerActionNames;
  payload?: any;
}

// initial state
export const initialCategoryManagerState: ICategoryManagerState = {
  categories: new Array<Category>(),
  error: null,
};

// reducer
export function categoryManagerModule(
  categoryManagerState: ICategoryManagerState = initialCategoryManagerState,
  action: CategoryManagerAction
): ICategoryManagerState {
  switch (action.type) {
    case CategoryManagerActionNames.GET_CATEGORIES_REQUEST: {
      return { ...categoryManagerState };
    }
    case CategoryManagerActionNames.GET_CATEGORIES_SUCCESS: {
      return {
        ...categoryManagerState,
        categories: [...action.payload],
      };
    }
    case CategoryManagerActionNames.CATEGORIES_ACTION_FAILED:
      return {
        ...categoryManagerState,
        error: action.payload,
      };
    default:
      return categoryManagerState;
  }
}
