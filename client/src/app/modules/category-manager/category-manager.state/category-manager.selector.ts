import { ICategoryManagerState } from './category-manager.state';
import { IAppState } from 'src/app/app.state/app.state';
import { createSelector } from '@ngrx/store';

const selectCategories = (state: IAppState) => state.categoryManager;

export const selectCategoryList = createSelector(
  selectCategories,
  (state: ICategoryManagerState) => state.categories
);
