import { Category } from '../../shared/model/category/category';

export interface ICategoryManagerState {
  categories: Category[];
  error: any;
}
