import { Action } from '@ngrx/store';
export enum LoadingEffectActionNames {
  OPEN_LOADING_STATE = '[LOADING EFFECT] Open Loading State',
  CLOSE_LOADING_STATE = '[LOADING EFFECT] Close Loading State',
}

export class OpenLoadingStateAction implements Action {
  type = LoadingEffectActionNames.OPEN_LOADING_STATE;
  constructor() {}
}
export class CloseLoadingStateAction implements Action {
  type = LoadingEffectActionNames.CLOSE_LOADING_STATE;
  constructor() {}
}
