import { ILoadingEffectState } from './loading-effect.state';
import { createSelector } from '@ngrx/store';
import { IAppState } from 'src/app/app.state/app.state';

const selectProducts = (state: IAppState) => state.loadingState;

export const selectLoadingState = createSelector(
  selectProducts,
  (state: ILoadingEffectState) => state.isLoading
);
