import { ILoadingEffectState } from './loading-effect.state';
import { Action } from '@ngrx/store';
import { LoadingEffectActionNames } from './loading-effect.action';

export interface LoadingEffectActionName extends Action {
  type: LoadingEffectActionNames;
  payload?: any;
}

// initial state
export const initialLoadingEffectState: ILoadingEffectState = {
  isLoading: false,
};

// reducer
export function loadingEffectModule(
  loadingEffectState: ILoadingEffectState = initialLoadingEffectState,
  action: LoadingEffectActionName
): ILoadingEffectState {
  switch (action.type) {
    case LoadingEffectActionNames.OPEN_LOADING_STATE: {
      return { ...loadingEffectState, isLoading: true };
    }
    case LoadingEffectActionNames.CLOSE_LOADING_STATE: {
      return { ...loadingEffectState, isLoading: false };
    }
    default:
      return loadingEffectState;
  }
}
