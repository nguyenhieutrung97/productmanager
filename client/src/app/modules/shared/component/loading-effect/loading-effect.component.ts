import { Observable } from 'rxjs';
import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { IAppState } from 'src/app/app.state/app.state';
import { selectLoadingState } from './loading-effect.state/loading-effect.selector';

@Component({
  selector: 'app-loading-effect',
  templateUrl: './loading-effect.component.html',
  styleUrls: ['./loading-effect.component.css'],
})
export class LoadingEffectComponent implements OnInit {
  isOpen$: Observable<boolean> = this.store.pipe(select(selectLoadingState));
  constructor(private store: Store<IAppState>) {}

  ngOnInit(): void {}
}
