import { ProductCreate } from 'src/app/modules/shared/model/product/productCreate';
import { Injectable } from '@angular/core';
import { BaseService } from './baseService';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { API_URI } from '../../../../environments/app.config';
import { Observable } from 'rxjs';
import { Product } from '../model/product/product';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ProductService extends BaseService {
  constructor(protected http: HttpClient) {
    super(http);
    this.baseUrl = (environment.ApiUrl || '') + API_URI.product;
  }
  getProducts(): Observable<Product[]> {
    const url = '';
    return this.get<Product[]>(url);
  }
  createProduct(product: ProductCreate): Observable<Product> {
    const url = '';
    console.log(product);
    return this.post<Product>(url, product);
  }
  updateProduct(id: string, product: ProductCreate): Observable<Product> {
    const url = `/${id}`;
    return this.put<Product>(url, product);
  }
  deleteProduct(id: string): Observable<Product> {
    const url = `/${id}`;
    return this.delete<Product>(url);
  }
}
