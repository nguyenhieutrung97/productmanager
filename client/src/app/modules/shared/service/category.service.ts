import { Injectable } from '@angular/core';
import { BaseService } from './baseService';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { API_URI } from 'src/environments/app.config';
import { Observable } from 'rxjs';
import { Category } from '../model/category/category';

@Injectable({
  providedIn: 'root',
})
export class CategoryService extends BaseService {
  constructor(protected http: HttpClient) {
    super(http);
    this.baseUrl = (environment.ApiUrl || '') + API_URI.category;
  }
  getCategories(): Observable<Category[]> {
    const url = '';
    return this.get<Category[]>(url);
  }
}
