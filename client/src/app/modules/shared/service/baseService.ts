import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';

@Injectable()
export class BaseService {
  protected baseUrl: string;

  constructor(protected http: HttpClient) {
    this.baseUrl = environment.ApiUrl;
  }

  protected get<Type>(url: string, options?: any): Observable<Type> {
    return this.http
      .get<Type>(this.baseUrl + url)
      .pipe(catchError(this.handleError));
  }

  protected post<Type>(
    url: string,
    data: any,
    options?: any
  ): Observable<Type> {
    return this.http
      .post<Type>(this.baseUrl + url, data)
      .pipe(catchError(this.handleError));
  }

  protected put<Type>(url: string, data: any): Observable<Type> {
    return this.http
      .put<Type>(this.baseUrl + url, data)
      .pipe(catchError(this.handleError));
  }

  protected delete<Type>(url: string, data?: Type) {
    return this.http
      .delete<Type>(this.baseUrl + url, data)
      .pipe(catchError(this.handleError));
  }

  protected handleError(error: HttpErrorResponse) {
    console.log(error);
    return throwError(error);
  }
}
