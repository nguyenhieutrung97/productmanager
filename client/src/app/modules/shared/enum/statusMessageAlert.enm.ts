export enum StatusMessageAlert {
  Error = 'Error',
  Warning = 'Warning',
  Success = 'Success',
  Unknown = 'Unknown',
}
