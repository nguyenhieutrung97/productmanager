import { Category } from '../category/category';

export class Product {
  constructor(
    id?: string,
    name?: string,
    description?: string,
    category?: Category
  ) {
    this.id = id ?? '';
    this.name = name ?? '';
    this.description = description ?? '';
    this.category = category ?? new Category();
  }
  public id: string;
  public name: string;
  public description: string;
  public category: Category;
}
