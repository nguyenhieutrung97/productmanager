export class ProductCreate {
  constructor(
    id?: string,
    name?: string,
    description?: string,
    categoryId?: string
  ) {
    this.id = id ?? '';
    this.name = name ?? '';
    this.description = description ?? '';
    this.categoryId = categoryId ?? '';
  }
  public id: string;
  public name: string;
  public description: string;
  public categoryId: string;
}
