import { StatusMessageAlert } from '../../enum/statusMessageAlert.enm';

export class MessageAlert {
  constructor(status?: StatusMessageAlert, message?: string) {
    (this.status = status ?? StatusMessageAlert.Unknown),
      (this.message = message ?? '');
  }
  status: StatusMessageAlert;
  message: string;
}
