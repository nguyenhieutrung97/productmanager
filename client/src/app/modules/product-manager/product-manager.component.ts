import { selectProductList } from './product-manager.state/product-manager.selector';
import { IAppState } from './../../app.state/app.state';
import { ProductCreate } from 'src/app/modules/shared/model/product/productCreate';
import { Product } from './../shared/model/product/product';
import { IProductManagerState } from './product-manager.state/product-manager.state';
import { Observable, Subscription } from 'rxjs';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { ProductService } from '../shared/service/product.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Store, select } from '@ngrx/store';
import {
  GetProductsActionRequest,
  ChangeStateCreateModal,
  ChangeStateUpdateModal,
  FlagDisplayProductModal,
  ChangeProductModal,
  DeleteProductActionRequest,
} from './product-manager.state/product-manager.action';
import { OpenProductManagerModal } from './shared/product-modal-event';
import { ToastrService } from 'ngx-toastr';
import { StatusMessageAlert } from '../shared/enum/statusMessageAlert.enm';
import { OpenLoadingStateAction } from '../shared/component/loading-effect/loading-effect.state/loading-effect.action';

@Component({
  selector: 'app-product-manager',
  templateUrl: './product-manager.component.html',
  styleUrls: ['./product-manager.component.css'],
})
export class ProductManagerComponent implements OnInit {
  products$: Observable<Product[]> = this.store.pipe(select(selectProductList));

  constructor(private store: Store<IAppState>, private toastr: ToastrService) {}
  subscriptionError: Subscription;
  ngOnInit(): void {
    this.store.dispatch(new GetProductsActionRequest());

    this.subscriptionError = this.store
      .select((state) => state.productManager.messageAlert)
      .subscribe((messageAlert) => {
        if (messageAlert.status === StatusMessageAlert.Error) {
          this.toastr.error(messageAlert.message);
        } else if (messageAlert.status === StatusMessageAlert.Success) {
          this.toastr.success(messageAlert.message);
        }
      });
  }
  ngOnDestroy() {
    this.subscriptionError.unsubscribe();
  }
  subscriptionProduct: Subscription;
  onClickEditButton(id: string) {
    let product: Product;
    this.subscriptionProduct = this.products$.subscribe((response) => {
      product = response.find((p) => p.id === id);
    });
    this.store.dispatch(new ChangeStateUpdateModal());
    this.store.dispatch(
      new ChangeProductModal(
        new ProductCreate(
          product.id,
          product.name,
          product.description,
          product.category.id
        )
      )
    );
    this.store.dispatch(new FlagDisplayProductModal(true));
    OpenProductManagerModal();
    this.subscriptionProduct.unsubscribe();
  }
  onClickDeleteButton(id) {
    this.store.dispatch(new DeleteProductActionRequest(id));
  }
  onClickCreateButton() {
    this.store.dispatch(new ChangeStateCreateModal());
    this.store.dispatch(new FlagDisplayProductModal(true));
    OpenProductManagerModal();
  }
}
