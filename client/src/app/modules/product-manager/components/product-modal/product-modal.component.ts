import { map } from 'rxjs/operators';
import { selectProductModal } from './../../product-manager.state/product-manager.selector';
import { selectCategoryList } from './../../../category-manager/category-manager.state/category-manager.selector';
import { Product } from './../../../shared/model/product/product';
import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {
  IProductManagerState,
  IProductModalState,
} from '../../product-manager.state/product-manager.state';
import { StatusModal } from 'src/app/modules/shared/enum/statusModal.enum';
import { IdModal } from 'src/app/modules/shared/enum/idModal.enum';
import {
  OpenProductManagerModal,
  CloseProductManagerModal,
} from '../../shared/product-modal-event';
import {
  FlagDisplayProductModal,
  ChangeStateCreateModal,
  AddProductActionRequest,
  UpdateProductActionRequest,
} from '../../product-manager.state/product-manager.action';
import { ICategoryManagerState } from 'src/app/modules/category-manager/category-manager.state/category-manager.state';
import { Category } from 'src/app/modules/shared/model/category/category';
import { Observable, Subscription } from 'rxjs';
import { GetCategoriesActionRequest } from 'src/app/modules/category-manager/category-manager.state/category-manager.action';
import { FormBuilder, Validators } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { ProductCreate } from 'src/app/modules/shared/model/product/productCreate';
import { productManagerModule } from '../../product-manager.state/product-manager.reducer';
import { ToastrService } from 'ngx-toastr';
import { IAppState } from 'src/app/app.state/app.state';

@Component({
  selector: 'app-product-modal',
  templateUrl: './product-modal.component.html',
  styleUrls: ['./product-modal.component.css'],
})
export class ProductModalComponent implements OnInit {
  categories$: Observable<Category[]> = this.store.pipe(
    select(selectCategoryList)
  );
  productModalTitle: StatusModal;
  idModal: string = IdModal.ProductModalManager;

  productModalForm = this.fb.group({
    id: ['', Validators.required],
    name: ['', Validators.required],
    categoryId: ['', Validators.required],
    description: ['', Validators.required],
  });
  get id() {
    return this.productModalForm.get('id');
  }
  get name() {
    return this.productModalForm.get('name');
  }
  get categoryId() {
    return this.productModalForm.get('categoryId');
  }
  get description() {
    return this.productModalForm.get('description');
  }

  constructor(
    private toastr: ToastrService,
    private fb: FormBuilder,
    private store: Store<IAppState>
  ) {}
  isModalOpen = false;
  subscriptions: Subscription[] = [];
  productModalSubscription: Subscription;
  productModalTitleSubscription: Subscription;
  ngOnInit(): void {
    this.store.dispatch(new GetCategoriesActionRequest());
    this.productModalSubscription = this.store
      .pipe(select(selectProductModal))
      .subscribe((productModal) => {
        this.productModalForm.patchValue({
          id: productModal.product.id,
          name: productModal.product.name,
          categoryId: productModal.product.categoryId,
          description: productModal.product.description,
        });
      });
    this.productModalTitleSubscription = this.store
      .pipe(select(selectProductModal))
      .subscribe((productModal) => {
        this.productModalTitle = productModal.typeModal;
      });
  }

  ngAfterViewChecked() {
    if (this.isModalOpen ) {
      OpenProductManagerModal();
    }
  }
  ngOnDestroy() {
    console.log('modal destroy');
    this.subscriptions.push(
      this.productModalSubscription,
      this.productModalTitleSubscription
    );
    this.unSubscriptions();
  }

  categoriesSubscription: Subscription;
  handleEditRequest() {
    const productCreate: ProductCreate = this.productModalForm.value;
    let category: Category;
    this.categoriesSubscription = this.categories$.subscribe((categories) => {
      category = categories.find((c) => c.id === productCreate.categoryId);
    });
    const product: Product = new Product(
      productCreate.id,
      productCreate.name,
      productCreate.description,
      category
    );
    this.store.dispatch(new UpdateProductActionRequest(product));
    this.subscriptions.push(this.categoriesSubscription);
    this.categoriesSubscription.unsubscribe();
  }
  handleCreateRequest() {
    this.store.dispatch(
      new AddProductActionRequest(this.productModalForm.value)
    );
  }
  wasSubmitted = false;
  onSubmit() {
    this.wasSubmitted = true;
    if (this.productModalForm.invalid) {
      this.toastr.error('Please fill all requires input');
      return false;
    }
    if (this.productModalTitle === StatusModal.Edit) {
      this.handleEditRequest();
    } else if (this.productModalTitle === StatusModal.Create) {
      this.handleCreateRequest();
    }
  }
  hideModal() {
    this.store.dispatch(new FlagDisplayProductModal(false));
    CloseProductManagerModal();
  }
  unSubscriptions() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  isReadonlyRequired() {
    return this.productModalTitle === StatusModal.Edit;
  }
}
