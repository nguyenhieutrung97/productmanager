import { ProductCreate } from 'src/app/modules/shared/model/product/productCreate';
import { createAction, props, Action } from '@ngrx/store';
import { Product } from '../../shared/model/product/product';
import { HttpEvent } from '@angular/common/http';
export enum ProductManagerActionNames {
  GET_PRODUCTS_REQUEST = '[PRODUCT MANAGER] Get Products Request',
  GET_PRODUCTS_SUCCESS = '[PRODUCT MANAGER] Get Products Success',
  GET_PRODUCT_REQUEST = '[PRODUCT MANAGER] Get Product Request',
  GET_PRODUCT_SUCCESS = '[PRODUCT MANAGER] Get Product Success',
  ADD_PRODUCT_REQUEST = '[PRODUCT MANAGER] Add Product Request',
  ADD_PRODUCT_SUCCESS = '[PRODUCT MANAGER] Add Product Success',
  DELETE_PRODUCT_REQUEST = '[PRODUCT MANAGER] Delete Product Request',
  DELETE_PRODUCT_SUCCESS = '[PRODUCT MANAGER] Delete Product Success',
  UPDATE_PRODUCT_REQUEST = '[PRODUCT MANAGER] Update Product Request',
  UPDATE_PRODUCT_SUCCESS = '[PRODUCT MANAGER] Update Product Success',
  PRODUCTS_ACTION_FAILED = '[PRODUCT MANAGER] Get Products Failed',
}
export enum ProductModalActionNames {
  CHANGE_STATE_CREATE_PRODUCT_MODAL = '[PRODUCT MODAL] Change Create State Product Modal',
  CHANGE_STATE_UPDATE_PRODUCT_MODAL = '[PRODUCT MODAL] Change Update State Product Modal',
  CHANGE_PRODUCT_PRODUCT_MODAL = '[PRODUCT MODAL] Change Product Product Modal',
  FLAG_DISPLAY_PRODUCT_MODAL = '[PRODUCT MODAL] Flag Display Product Modal',
}

export class GetProductActionSuccess implements Action {
  type = ProductManagerActionNames.GET_PRODUCT_SUCCESS;
  constructor(public payload: Product) {}
}

export class GetProductsActionRequest implements Action {
  type = ProductManagerActionNames.GET_PRODUCTS_REQUEST;
  constructor() {}
}
export class GetProductsActionSuccess implements Action {
  type = ProductManagerActionNames.GET_PRODUCTS_SUCCESS;
  constructor(public payload: Product[]) {}
}

export class AddProductActionRequest implements Action {
  type = ProductManagerActionNames.ADD_PRODUCT_REQUEST;
  constructor(public payload: ProductCreate) {}
}
export class AddProductActionSuccess implements Action {
  type = ProductManagerActionNames.ADD_PRODUCT_SUCCESS;
  constructor(public payload: Product) {}
}
export class DeleteProductActionRequest implements Action {
  type = ProductManagerActionNames.DELETE_PRODUCT_REQUEST;
  constructor(public payload: string) {}
}
export class DeleteProductActionSuccess implements Action {
  type = ProductManagerActionNames.DELETE_PRODUCT_SUCCESS;
  constructor(public payload: string) {}
}
export class UpdateProductActionRequest implements Action {
  type = ProductManagerActionNames.UPDATE_PRODUCT_REQUEST;
  constructor(public payload: Product) {}
}
export class UpdateProductActionSuccess implements Action {
  type = ProductManagerActionNames.UPDATE_PRODUCT_SUCCESS;
  constructor(public payload: Product) {}
}

export class ProductActionFailed implements Action {
  type = ProductManagerActionNames.PRODUCTS_ACTION_FAILED;
  constructor(public payload: any) {}
}
export class ChangeStateCreateModal implements Action {
  type = ProductModalActionNames.CHANGE_STATE_CREATE_PRODUCT_MODAL;
  constructor() {}
}
export class ChangeStateUpdateModal implements Action {
  type = ProductModalActionNames.CHANGE_STATE_UPDATE_PRODUCT_MODAL;
  constructor() {}
}
export class ChangeProductModal implements Action {
  type = ProductModalActionNames.CHANGE_PRODUCT_PRODUCT_MODAL;
  constructor(public payload: ProductCreate) {}
}
export class FlagDisplayProductModal implements Action {
  type = ProductModalActionNames.FLAG_DISPLAY_PRODUCT_MODAL;
  constructor(public payload: boolean) {}
}
