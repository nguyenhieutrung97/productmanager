import { IProductManagerState } from './product-manager.state';
import { IAppState } from './../../../app.state/app.state';
import { createSelector } from '@ngrx/store';

const selectProducts = (state: IAppState) => state.productManager;

export const selectProductList = createSelector(
  selectProducts,
  (state: IProductManagerState) => state.products
);

export const selectProductModal = createSelector(
  selectProducts,
  (state: IProductManagerState) => state.productModal
);

export const messageProductManager = createSelector(
  selectProducts,
  (state: IProductManagerState) => state.messageAlert
);
