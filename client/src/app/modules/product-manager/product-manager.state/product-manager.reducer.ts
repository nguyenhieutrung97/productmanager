import { Action } from '@ngrx/store';
import {
  ProductManagerActionNames,
  ProductModalActionNames,
} from './product-manager.action';
import {
  IProductManagerState,
  IProductModalState,
} from './product-manager.state';
import { Product } from '../../shared/model/product/product';
import { ProductCreate } from '../../shared/model/product/productCreate';
import { StatusModal } from '../../shared/enum/statusModal.enum';
import { MessageAlert } from '../../shared/model/message-alert/message-alert';
import { StatusMessageAlert } from '../../shared/enum/statusMessageAlert.enm';

export interface ProductManagerAction extends Action {
  type: ProductManagerActionNames | ProductModalActionNames;
  payload?: any;
}

// initial state
export const initialProductManagerState: IProductManagerState = {
  products: new Array<Product>(),
  productModal: {
    product: new ProductCreate(),
    typeModal: StatusModal.Create,
    isOpen: false,
  },
  messageAlert: new MessageAlert(),
};

// reducer
export function productManagerModule(
  productManagerState: IProductManagerState = initialProductManagerState,
  action: ProductManagerAction
): IProductManagerState {
  switch (action.type) {
    case ProductManagerActionNames.GET_PRODUCTS_REQUEST:
      return { ...productManagerState };
    case ProductManagerActionNames.GET_PRODUCTS_SUCCESS:
      return {
        ...productManagerState,
        products: [...action.payload],
      };
    case ProductManagerActionNames.ADD_PRODUCT_REQUEST:
      return {
        ...productManagerState,
      };
    case ProductManagerActionNames.ADD_PRODUCT_SUCCESS:
      return {
        ...productManagerState,
        products: [action.payload, ...productManagerState.products],
        messageAlert: new MessageAlert(
          StatusMessageAlert.Success,
          'Add product successful!!!'
        ),
      };
    case ProductManagerActionNames.DELETE_PRODUCT_REQUEST:
      return {
        ...productManagerState,
      };
    case ProductManagerActionNames.DELETE_PRODUCT_SUCCESS:
      return {
        ...productManagerState,

        products: [
          ...productManagerState.products.filter(
            (product) => product.id !== action.payload
          ),
        ],
        messageAlert: new MessageAlert(
          StatusMessageAlert.Success,
          'Delete product successful!!!'
        ),
      };
    case ProductManagerActionNames.UPDATE_PRODUCT_REQUEST:
      return {
        ...productManagerState,
      };
    case ProductManagerActionNames.UPDATE_PRODUCT_SUCCESS:
      return {
        ...productManagerState,
        products: [
          ...productManagerState.products.map((product) => {
            if (product.id !== action.payload.id) {
              return product;
            } else {
              return action.payload;
            }
          }),
        ],
        messageAlert: new MessageAlert(
          StatusMessageAlert.Success,
          'Update product successful!!!'
        ),
      };
    case ProductManagerActionNames.PRODUCTS_ACTION_FAILED:
      return {
        ...productManagerState,
        messageAlert: new MessageAlert(
          StatusMessageAlert.Error,
          action.payload
        ),
      };

    case ProductModalActionNames.CHANGE_STATE_CREATE_PRODUCT_MODAL: {
      let productModal: IProductModalState;

      if (productManagerState.productModal.typeModal === StatusModal.Edit) {
        productModal = {
          ...productManagerState.productModal,
          product: new ProductCreate(),
          typeModal: StatusModal.Create,
        };
      } else {
        productModal = {
          ...productManagerState.productModal,
          typeModal: StatusModal.Create,
        };
      }

      return { ...productManagerState, productModal: { ...productModal } };
    }
    case ProductModalActionNames.CHANGE_STATE_UPDATE_PRODUCT_MODAL: {
      const productModal: IProductModalState = {
        ...productManagerState.productModal,
        typeModal: StatusModal.Edit,
      };
      return { ...productManagerState, productModal: { ...productModal } };
    }
    case ProductModalActionNames.CHANGE_PRODUCT_PRODUCT_MODAL: {
      const productModal: IProductModalState = {
        ...productManagerState.productModal,
        product: { ...action.payload },
      };
      return { ...productManagerState, productModal: { ...productModal } };
    }
    case ProductModalActionNames.FLAG_DISPLAY_PRODUCT_MODAL: {
      const productModal: IProductModalState = {
        ...productManagerState.productModal,
        isOpen: action.payload,
      };
      return { ...productManagerState, productModal: { ...productModal } };
    }
    default:
      return productManagerState;
  }
}
