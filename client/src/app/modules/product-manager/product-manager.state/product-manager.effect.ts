import { GetCategoriesActionRequest } from './../../category-manager/category-manager.state/category-manager.action';
import {
  OpenLoadingStateAction,
  CloseLoadingStateAction,
} from './../../shared/component/loading-effect/loading-effect.state/loading-effect.action';
import { ofType, Actions, createEffect } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import {
  map,
  catchError,
  mergeMap,
  exhaustMap,
  finalize,
} from 'rxjs/operators';
import {
  ProductManagerActionNames,
  GetProductsActionSuccess,
  ProductActionFailed,
  AddProductActionSuccess,
  AddProductActionRequest,
  UpdateProductActionRequest,
  UpdateProductActionSuccess,
  DeleteProductActionRequest,
  DeleteProductActionSuccess,
  ProductModalActionNames,
} from './product-manager.action';

import { ProductService } from './../../shared/service/product.service';
import { of, Observable, empty } from 'rxjs';
import { ProductCreate } from '../../shared/model/product/productCreate';
import { HttpErrorResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { IAppState } from 'src/app/app.state/app.state';

@Injectable()
export class ProductManagerEffects {
  constructor(
    private actions$: Actions,
    private store: Store<IAppState>,
    private productService: ProductService
  ) {}

  loadProducts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ProductManagerActionNames.GET_PRODUCTS_REQUEST),
      mergeMap(() => {
        this.store.dispatch(new OpenLoadingStateAction());
        return this.productService.getProducts().pipe(
          map((products) => new GetProductsActionSuccess(products)),
          catchError((error: HttpErrorResponse) =>
            of(new ProductActionFailed(error.message))
          ),
          finalize(() => {
            this.store.dispatch(new CloseLoadingStateAction());
          })
        );
      })
    )
  );
  createProducts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ProductManagerActionNames.ADD_PRODUCT_REQUEST),
      exhaustMap((action: AddProductActionRequest) => {
        this.store.dispatch(new OpenLoadingStateAction());
        return this.productService.createProduct(action.payload).pipe(
          map((product) => new AddProductActionSuccess(product)),
          catchError((error: HttpErrorResponse) => {
            if (error.status === 400) {
              return of(new ProductActionFailed('Duplicate Id!!!'));
            } else {
              return of(new ProductActionFailed(error.message));
            }
          }),
          finalize(() => {
            this.store.dispatch(new CloseLoadingStateAction());
          })
        );
      })
    )
  );
  updateProduct$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ProductManagerActionNames.UPDATE_PRODUCT_REQUEST),
      exhaustMap((action: UpdateProductActionRequest) => {
        this.store.dispatch(new OpenLoadingStateAction());
        return this.productService
          .updateProduct(
            action.payload.id,
            new ProductCreate(
              action.payload.id,
              action.payload.name,
              action.payload.description,
              action.payload.category.id
            )
          )
          .pipe(
            map(() => new UpdateProductActionSuccess(action.payload)),
            catchError((error: HttpErrorResponse) => {
              if (error.status === 400) {
                return of(new ProductActionFailed('Duplicate Id!!!'));
              } else {
                return of(new ProductActionFailed(error.message));
              }
            }),
            finalize(() => {
              this.store.dispatch(new CloseLoadingStateAction());
            })
          );
      })
    )
  );
  deleteProducts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ProductManagerActionNames.DELETE_PRODUCT_REQUEST),
      exhaustMap((action: DeleteProductActionRequest) => {
        this.store.dispatch(new OpenLoadingStateAction());
        return this.productService.deleteProduct(action.payload).pipe(
          map(() => new DeleteProductActionSuccess(action.payload)),
          catchError((error: HttpErrorResponse) => {
            if (error.status === 404) {
              return of(new ProductActionFailed('Id Not Exist!!!'));
            } else {
              return of(new ProductActionFailed(error.message));
            }
          }),
          finalize(() => {
            this.store.dispatch(new CloseLoadingStateAction());
          })
        );
      })
    )
  );
  openProductModal$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        ProductModalActionNames.CHANGE_STATE_CREATE_PRODUCT_MODAL ||
          ProductModalActionNames.CHANGE_STATE_UPDATE_PRODUCT_MODAL
      ),
      map(() => {
        return new GetCategoriesActionRequest();
      })
    )
  );
}
