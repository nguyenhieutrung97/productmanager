import { Product } from '../../shared/model/product/product';
import { ProductCreate } from '../../shared/model/product/productCreate';
import { StatusModal } from '../../shared/enum/statusModal.enum';
import { MessageAlert } from '../../shared/model/message-alert/message-alert';

export interface IProductModalState {
  product: ProductCreate;
  typeModal: StatusModal;
  isOpen: boolean;
}
export interface IProductManagerState {
  products: Product[];
  productModal: IProductModalState;
  messageAlert: MessageAlert;
}
