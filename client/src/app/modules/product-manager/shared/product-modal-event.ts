import { IdModal } from 'src/app/modules/shared/enum/idModal.enum';
declare const bootstrap: any;
let productManagerModalRef;
export const OpenProductManagerModal = () => {
  console.log('product-modal open');
  if(productManagerModalRef===undefined){
    InitialProductManagerModal();
  }
  productManagerModalRef.show();
};
export const CloseProductManagerModal = () => {
  console.log('product-modal hide');
  productManagerModalRef.hide();
};
const InitialProductManagerModal = () => {
  productManagerModalRef = new bootstrap.Modal(
    document.getElementById(IdModal.ProductModalManager)
  );
};

