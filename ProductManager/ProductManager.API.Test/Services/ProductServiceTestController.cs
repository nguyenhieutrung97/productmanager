﻿using Microsoft.Extensions.Logging;
using Moq;
using Neo4jClient;
using Neo4jClient.Cypher;
using ProductManager.Domain.Dto;
using ProductManager.Infrastructure.Repositories;
using ProductManager.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ProductManager.API.Test.Services
{
    public class ProductServiceTestController
    {
        private string idExist = "2";
        private string idNotExist = "12";
        private readonly ProductCreateDto productCreate;
        private readonly ProductCreateDto productUpdate;
        private readonly ProductCreateDto productDupplicateId;
        private readonly List<ProductDto> productList;
        private readonly List<CategoryDto> categoryList;

        private readonly Mock<IBoltGraphClient> _graphClientMock;
        private readonly Mock<ICypherFluentQuery> _mockCypherFluentQueryFake;
       
        IProductService productService;
        Mock<IProductRepository> _productRepository;
        Mock<ILogger<ProductService>> _logger;
        public ProductServiceTestController()
        {

            categoryList = new List<CategoryDto>()
            {
                new CategoryDto()
                {
                    Id="1",Name="Smartphone"
                },
                new CategoryDto()
                {
                    Id="2",Name="Tablet"
                }
            };
            productCreate = new ProductCreateDto() { Id = "5", Name = "Hello Phone", Description = "Best phone 2020", CategoryId = "1" };
            productUpdate = new ProductCreateDto() { Id = "1", Name = "Galaxy S10", Description = "Flagship 2019 Samsung", CategoryId="1" };
            productDupplicateId = new ProductCreateDto() { Id = "1", Name = "Hello Phone", Description = "Best phone 2020", CategoryId = "1" };
            productList = new List<ProductDto>()
            {
                new ProductDto()
                {
                    Id="1",Name="Galaxy S10",Description="Flagship 2019",Category=categoryList[0]
                },
                new ProductDto()
                {
                    Id="2",Name="Razer 2020",Description="Flagship Laptop 2020",Category=categoryList[1]
                },
                new ProductDto()
                {
                    Id="3",Name="Msi Sonarlint",Description="Laptop bug resolved",Category=categoryList[1]
                },
                 new ProductDto()
                {
                    Id="4",Name="Apple XX",Description="Limited",Category=categoryList[0]
                }
            };
            _productRepository = new Mock<IProductRepository>();
            _productRepository.Setup(pR => pR.GetProductById(idExist)).Returns(Task.FromResult<ProductDto>(productList.Where(pL => pL.Id == idExist).FirstOrDefault()));
            _productRepository.Setup(pR => pR.GetProductById(idNotExist)).Returns(Task.FromResult<ProductDto>(productList.Where(pL => pL.Id == idNotExist).FirstOrDefault()));
            _productRepository.Setup(pR => pR.GetProducts()).Returns(Task.FromResult<IEnumerable<ProductDto>>(productList));
            var productCreateResult = new ProductDto() { Id = productCreate.Id, Name = productCreate.Name, Description = productCreate.Description, Category = categoryList.Where(cL => cL.Id == productCreate.CategoryId).FirstOrDefault() };
            _productRepository.Setup(pR => pR.CreateProduct(productCreate)).Returns(Task.FromResult<ProductDto>(productCreateResult));
            _productRepository.Setup(pR => pR.CreateProduct(productDupplicateId)).Returns(Task.FromResult<ProductDto?>(null));
            _productRepository.Setup(pR => pR.UpdateProduct(productUpdate.Id,productUpdate)).Returns(Task.FromResult<bool>(true));
            _productRepository.Setup(pR => pR.UpdateProduct(productUpdate.Id, productDupplicateId)).Returns(Task.FromResult<bool>(false));
            _logger = new Mock<ILogger<ProductService>>();
            productService = new ProductService(_productRepository.Object, _logger.Object);
        }
        [Fact]
        public async Task GetProduct_ReturnProduct_WhenSuccess()
        {
            var result = await productService.GetProductById(idExist);
            Assert.IsType<ProductDto>(result);
            Assert.Equal(idExist, result.Id);
        }

        [Fact]
        public async Task GetProduct_ReturnNull_WhenIdNotFound()
        {
            var result = await productService.GetProductById(idNotExist);

            Assert.Null(result);
        }

        [Fact]
        public async Task GetProducts_ReturnProductList_WhenSuccess()
        {
            var result = await productService.GetProducts();

            Assert.IsType<List<ProductDto>>(result);
            Assert.Equal(4,result.ToList().Count);
        }

        [Fact]
        public async Task CreateProduct_ReturnProduct_WhenSuccess()
        {
            var result = await productService.CreateProduct(productCreate);

            Assert.IsType<ProductDto>(result);
            Assert.True(result.Id == productCreate.Id);
        }

        [Fact]
        public async Task CreateProduct_ReturnNull_WhenIdDupplicate()
        {
            var result = await productService.CreateProduct(productDupplicateId);

            Assert.True(result == null);
        }
        [Fact]
        public async Task UpdateProduct_ReturnData_WhenSuccess()
        {
            var result = await productService.UpdateProduct(productUpdate.Id,productUpdate);

            Assert.IsType<bool>(result);
            Assert.True(result);
        }

        [Fact]
        public async Task UpdateProduct_ReturnNull_WhenIdDupplicate()
        {
            var result = await productService.UpdateProduct(productDupplicateId.Id, productDupplicateId);

            Assert.False( false);
        }
    }
}
