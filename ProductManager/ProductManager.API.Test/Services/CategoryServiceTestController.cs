﻿using Moq;
using ProductManager.Domain.Dto;
using ProductManager.Infrastructure.Repositories;
using ProductManager.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ProductManager.API.Test.Services
{
    public class CategoryServiceTestController
    {
        private readonly List<CategoryDto> categoryList;
        ICategoryService categoryService;
        Mock<ICategoryRepository> _categoryRepository;
        public CategoryServiceTestController()
        {
            categoryList = new List<CategoryDto>()
            {
                new CategoryDto()
                {
                    Id="1",Name="Smartphone"
                },
                new CategoryDto()
                {
                    Id="2",Name="Tablet"
                }
            };
            _categoryRepository.Setup(pR => pR.GetCategories()).Returns(Task.FromResult<IEnumerable<CategoryDto>>(categoryList));
        }
        [Fact]
        public async Task GetCategories_ReturnCategoryList_WhenSuccess()
        {
            var result = await categoryService.GetCategories();
            Assert.IsType<List<CategoryDto>>(result);
            Assert.Equal(2, result.ToList().Count);
        }
    }
}
