
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Neo4j.Driver.V1;
using Neo4jClient;
using ProductManager.Domain.OptionsConfiguration;
using ProductManager.Infrastructure.Repositories;
using ProductManager.Services.Services;

namespace ProductManager.API
{
    public class Startup
    {
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<Neo4jSetting>(Configuration.GetSection("Neo4jSetting"));
            services.AddCors(options =>
            {
                options.AddPolicy(name: MyAllowSpecificOrigins,
                                  builder =>
                                  {
                                      builder.WithOrigins("http://localhost:4200").AllowAnyHeader().AllowAnyMethod();
                                  });
            });

            services.AddControllers();
            services.AddSwaggerGen();
            RegisterNeo4jDriver(services);
            RegisterGraphClient(services);
            RegisterRepositories(services);
            RegisterServices(services);
            services.AddApiVersioning(config =>
            {
                config.DefaultApiVersion = new ApiVersion(1, 0);
                config.AssumeDefaultVersionWhenUnspecified = true;
                config.ReportApiVersions = true;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseRouting();
            app.UseCors(MyAllowSpecificOrigins);

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private IServiceCollection RegisterRepositories(IServiceCollection services)
        {
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            return services;
        }

        private IServiceCollection RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<ICategoryService, CategoryService>();
            return services;
        }

        private IServiceCollection RegisterNeo4jDriver(IServiceCollection services)
        {
            services.AddSingleton(typeof(IDriver), resolver =>
            {
                var neo4jSetting = Configuration.GetSection("Neo4jSetting").Get<Neo4jSetting>();
                var authToken = AuthTokens.Basic(neo4jSetting.UserName, neo4jSetting.Password);
                var driver = GraphDatabase.Driver(neo4jSetting.BoltUrl, authToken, Config.Builder.WithEncryptionLevel(EncryptionLevel.None).ToConfig());
                return driver;
            });
            return services;
        }

        private IServiceCollection RegisterGraphClient(IServiceCollection services)
        {
            services.AddSingleton(typeof(IBoltGraphClient), resolver =>
            {
                var client = new BoltGraphClient(services.BuildServiceProvider().GetService<IDriver>());

                if (!client.IsConnected)
                {
                    client.Connect();
                }

                return client;
            });
            return services;
        }
    }
}
