﻿using Microsoft.AspNetCore.Mvc;
using ProductManager.Domain.Dto;
using ProductManager.Services.Services;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace CategoryManager.API.Controllers
{
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/categories")]
    [ApiController]
    [ApiVersion("1.0")]
    public class CategoriesController:ControllerBase
    {
        private readonly ICategoryService _categoryService;

        public CategoriesController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [HttpGet]
        public async Task<ActionResult<List<CategoryDto>>> GetCategories()
        {
            var categories = await _categoryService.GetCategories();
            return Ok(categories);
        }

    }
}
