﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProductManager.Domain.Dto;
using ProductManager.Services.Services;

namespace ProductManager.API.Controllers
{
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/products")]
    [ApiController]
    [ApiVersion("1.0")]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductsController(IProductService productService)
        {
            _productService = productService;
        }
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [HttpGet]
        public async Task<ActionResult<List<ProductDto>>> GetProducts()
        {
            var products = await _productService.GetProducts();
            return Ok(products);
        }

        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [HttpGet("{id}")]
        public async Task<ActionResult<ProductDto>> GetProduct(string id)
        {

            var product = await _productService.GetProductById(id);

            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);
        }

        // PUT: api/Products/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NoContent)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduct(string id, ProductCreateDto product)
        {
            var isProductUpdated = false;
            if (ModelState.IsValid)
            {
                isProductUpdated = await _productService.UpdateProduct(id, product);
            }

            if (!isProductUpdated)
            {
                return BadRequest();
            }
            else
            {
                return NoContent();
            }
        }
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Created)]
        [HttpPost]
        public async Task<ActionResult<ProductDto>> PostProduct(ProductCreateDto productDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var product = await _productService.CreateProduct(productDto);
            if (product == null)
            {
                return BadRequest();
            }
            return CreatedAtAction("GetProduct", new { id = product.Id, version = HttpContext.GetRequestedApiVersion().ToString() }, product);
        }

        // DELETE: api/Products/5
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteProduct(string id)
        {
            var isProductDeleted = await _productService.DeleteProduct(id);
            if (!isProductDeleted)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}