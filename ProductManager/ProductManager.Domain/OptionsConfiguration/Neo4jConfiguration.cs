﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductManager.Domain.OptionsConfiguration
{
    public class Neo4jSetting
    {
        public string BoltUrl { get; set; }
        public string DbUrl { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
