﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductManager.Domain.Constants
{
    public static class Labels
    {
        public const string PRODUCT = "Product";
        public const string CATEGORY = "Category";
    }
}
