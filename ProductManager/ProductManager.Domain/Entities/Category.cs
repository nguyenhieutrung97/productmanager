﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductManager.Domain.Entities
{
    public class Category:BaseNode
    {
        public const string LABEL_CATEGORY = "Category";
        public Category():base(LABEL_CATEGORY) {}
        public string Name { get; set; }
    }
}
