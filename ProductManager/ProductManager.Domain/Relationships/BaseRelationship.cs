﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductManager.Domain.Relationships
{
    public abstract class BaseRelationship
    {
        private readonly string _label;

        public BaseRelationship(string label)
        {
            _label = !string.IsNullOrWhiteSpace(label) ? label : this.GetType().Name;
        }



        public string Label => _label;
    }
}
