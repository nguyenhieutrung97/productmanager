﻿using Neo4jClient;
using Neo4jClient.Cypher;
using ProductManager.Domain.Dto;
using ProductManager.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManager.Infrastructure.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {

        private readonly IBoltGraphClient _graphClient;

        public CategoryRepository(IBoltGraphClient graphClient)
        {
            _graphClient = graphClient;
        }
        public async Task<IEnumerable<CategoryDto>> GetCategories()
        {
            var categories = await _graphClient.Cypher.Read.Match($"(category:{Category.LABEL_CATEGORY})").Return((category)=>category.As<CategoryDto>()).ResultsAsync;
            return categories;
        }
    }
}
