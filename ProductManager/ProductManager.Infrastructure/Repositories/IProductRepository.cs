﻿using ProductManager.Domain.Dto;
using ProductManager.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProductManager.Infrastructure.Repositories
{
    public interface IProductRepository
    {
        public Task<IEnumerable<ProductDto>>  GetProducts();
        public Task<ProductDto> GetProductById(string id);
        public Task<ProductDto> CreateProduct(ProductCreateDto product);
        public Task<bool> UpdateProduct(string id, ProductCreateDto product);
        public Task<bool> DeleteProduct(string id);
    }
}
