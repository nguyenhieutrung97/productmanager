﻿using ProductManager.Domain.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProductManager.Infrastructure.Repositories
{
    public interface ICategoryRepository
    {
        public Task<IEnumerable<CategoryDto>> GetCategories();
    }
}
