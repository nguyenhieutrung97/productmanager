﻿using Neo4jClient;
using Neo4jClient.Cypher;
using ProductManager.Domain.Dto;
using ProductManager.Domain.Entities;
using ProductManager.Domain.Relationships;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ProductManager.Infrastructure.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly IBoltGraphClient _graphClient;
        
        public ProductRepository(IBoltGraphClient graphClient)
        {
            _graphClient = graphClient;
        }
        async Task<IEnumerable<ProductDto>> GroupProductAndCategory(ICypherFluentQuery expression)
        {
            return await expression.ReturnDistinct((product, category) => new ProductDto { Id = product.As<ProductDto>().Id, Name = product.As<ProductDto>().Name, Description = product.As<ProductDto>().Description, Category = category.As<CategoryDto>() }).ResultsAsync;
        }
        public async Task<ProductDto> CreateProduct(ProductCreateDto productCreateDto)
        {
            var query = _graphClient.Cypher.Match($"(category:{Category.LABEL_CATEGORY})")
                .Where("category.Id=$categoryId").WithParam("categoryId", productCreateDto.CategoryId)
                .Create($"(category)-[:{HasProduct.LABEL_HAS_PRODUCT}]->(product:{Product.LABEL_PRODUCT}{{ {nameof(Product.Id)}:'{productCreateDto.Id}',{nameof(Product.Name)}:'{productCreateDto.Name}',{nameof(Product.Description)}:'{productCreateDto.Description}' }})") ;
            var results = await GroupProductAndCategory(query);
            var product = results.FirstOrDefault();
            return product;
        }

        public async Task<ProductDto> GetProductById(string id)
        {
            var query = _graphClient.Cypher.Read.Match($"(product:{Product.LABEL_PRODUCT})<-[:{HasProduct.LABEL_HAS_PRODUCT}]-(category)")
                .Where("product.Id=$productId").WithParam("productId", id);
            var results = await GroupProductAndCategory(query);
            var product = results.FirstOrDefault();
            return product;
        }

        public async Task<IEnumerable<ProductDto>> GetProducts()
        {
            var query = _graphClient.Cypher.Read.Match($"(product:{Product.LABEL_PRODUCT})<-[:{HasProduct.LABEL_HAS_PRODUCT}]-(category)");
            var products = await GroupProductAndCategory(query);
            return products;
        }

        public async Task<bool> UpdateProduct(string id, ProductCreateDto productCreateDto)
        {
            var query = _graphClient.Cypher.Write.Match($"(:{Category.LABEL_CATEGORY})-[rel:{HasProduct.LABEL_HAS_PRODUCT}]->(product:{Product.LABEL_PRODUCT})")
                                                .Where("product.Id=$productId").WithParam("productId", id)
                                                .Set($"product.{nameof(Product.Name)}='{productCreateDto.Name}',product.{nameof(Product.Description)}='{productCreateDto.Description}'")
                                                .Delete("rel")
                                                .With("product")
                                                .Match($"(category:{Category.LABEL_CATEGORY})")
                                                .Where("category.Id=$categoryId").WithParam("categoryId", productCreateDto.CategoryId)
                                                .Merge($"(category)-[:{HasProduct.LABEL_HAS_PRODUCT}]->(product)");
            var products = await GroupProductAndCategory(query);
            return Enumerable.Any<ProductDto>(products);
        }
        public async Task<bool> DeleteProduct(string id)
        {
            var query = _graphClient.Cypher.Write.Match($"(category:{Category.LABEL_CATEGORY})-[rel:{HasProduct.LABEL_HAS_PRODUCT}]->(product:{Product.LABEL_PRODUCT})")
                                                .Where("product.Id=$productId").WithParam("productId", id)
                                                .Delete("rel")
                                                .Merge($"(category)-[:{HadProduct.LABEL_HAD_PRODUCT}]->(product)");
            var products = await GroupProductAndCategory(query);
            return Enumerable.Any<ProductDto>(products);
        }
    }
}
