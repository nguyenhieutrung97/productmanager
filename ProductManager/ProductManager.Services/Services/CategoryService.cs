﻿using ProductManager.Domain.Dto;
using ProductManager.Infrastructure.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManager.Services.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        public CategoryService(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }
        public async Task<IEnumerable<CategoryDto>> GetCategories()
        {
            return await _categoryRepository.GetCategories();
        }
    }
}
