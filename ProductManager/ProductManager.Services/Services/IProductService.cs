﻿using ProductManager.Domain.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManager.Services.Services
{
    public interface IProductService
    {
        public Task<IEnumerable<ProductDto>> GetProducts();
        public Task<ProductDto> GetProductById(string id);
        public Task<ProductDto> CreateProduct(ProductCreateDto product);
        public Task<bool> UpdateProduct(string id, ProductCreateDto product);
        public Task<bool> DeleteProduct(string id);
    }
}
