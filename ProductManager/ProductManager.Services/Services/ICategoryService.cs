﻿using ProductManager.Domain.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManager.Services.Services
{
    public interface ICategoryService
    {
        public Task<IEnumerable<CategoryDto>> GetCategories();
    }
}
