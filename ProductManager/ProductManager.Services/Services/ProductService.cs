﻿using Microsoft.Extensions.Logging;
using ProductManager.Domain.Dto;
using ProductManager.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManager.Services.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;
        private readonly ILogger _logger;
        public ProductService(IProductRepository productRepository, ILogger<ProductService> logger)
        {
            _productRepository = productRepository;
            _logger = logger;
        }

        public async Task<ProductDto> CreateProduct(ProductCreateDto product)
        {
            try
            {
                return await _productRepository.CreateProduct(product);
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, $"Cannot create product due to error server or dupplicate id. Error: {e.Message}", product);
                return null;
            }
        }

        public async Task<bool> DeleteProduct(string id)
        {
            return await _productRepository.DeleteProduct(id);
        }

        public async Task<ProductDto> GetProductById(string id)
        {
            return await _productRepository.GetProductById(id);
        }

        public async Task<IEnumerable<ProductDto>> GetProducts()
        {
            return await _productRepository.GetProducts();
        }

        public async Task<bool> UpdateProduct(string id, ProductCreateDto product)
        {
            try
            {
                return await _productRepository.UpdateProduct(id, product);
            }
            catch(Exception e)
            {
                _logger.Log(LogLevel.Error, $"Cannot update product due to error server or dupplicate id. Error: {e.Message}", product);
                return false;
            }
        }
    }
}
